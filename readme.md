# A custom Keycloak User Storage Provider

## Prepare
    Set Keycloak Home

## Build
Build the ear archive

    mvn clean install

## Prepare Keycloak
Run [setup.cli](./setup.cli) with

    echo "yes" | $KEYCLOAK_HOME/bin/jboss-cli.sh --file=$KEYCLOAK_HOME/setup.cli
This configures the custom logger and registers the static configuration 
of the user federation provider. 

## Start Keycloak
    
    cd $KEYCLOAK_HOME
    bin/standalone.sh -c standalone-ha.xml 

## Deployment
Deploy `.ear` to wildfly

    mvn wildfly:deploy -Djboss-as.home=$KEYCLOAK_HOME
