package pz.pse.keycloak.ext.storage.demo;

import org.keycloak.credential.CredentialInput;
import org.keycloak.models.RealmModel;

public interface UserModelInterface {
    UserModel getUserByUsername(String username, RealmModel realm);

    UserModel getUserById(String id, RealmModel realm);

    UserModel getUserByEmail(String email, RealmModel realm);

    boolean isConfiguredFor(RealmModel realm, UserModel user, String credentialType);

    boolean supportsCredentialType(String credentialType);

    boolean isValid(RealmModel realm, UserModel user, CredentialInput input);
}
