package pz.pse.keycloak.ext.storage.demo;

import org.keycloak.models.RoleMapperModel;

public interface UserModelInt extends RoleMapperModel {
    String getId();

    String getUsername();
    void setUsername(String username);

    String getFirstName();
    void setFirstName(String firstName);

    String getLastName();
    void setLastName(String lastName);

    String getEmail();
    void setEmail(String email);
}
